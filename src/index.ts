import { EventEmitter } from 'events';

export class TimeoutError extends Error {
  constructor(message: string) {
    super(message);
    this.name = 'TimeoutError';
  }
}

enum ONE {
  MS = 1,
  SECOND = 1000,
  MINUTE = 60 * 1000,
  HOUR = 60 * 60 * 1000,
  DAY = 24 * 60 * 60 * 1000,
  WEEK = 7 * 24 * 60 * 60 * 1000,
  MONTH = ONE.DAY * 30,
}

export const TIME = { ONE };

export type Resolve<T> = (t: T) => void;

export type Reject = (e: Error) => void;

export type FN<I, O> = (i: I, ...args: any[]) => O;

export type Either<E extends Error, T> = { error: E, result?: undefined } | { error?: undefined, result: T };

/**
 * helper functions that calls a function that returns a promise into a promise of Either type.
 * @param cb
 */
export function qd<T>(cb: () => Promise<T>): Promise<Either<Error, T>> {
  return cb().then((result) => ({ result })).catch((error) => ({ error }));
}

/**
 * helper method that generally used to transform callback-based API into promise-based.
 * @param cb :required
 * @param timeout :optional {number} in ms.
 * @example
 * ```
 *  import { q } from '@cryptoket/ts-promise-helper;
 *  import fs from 'fs';
 *  async function test() {
 *    const result = await q((next) => fs.readFile('myfile', next));
 *  }
 * ```
 */
export function q<T extends any = any | undefined | null>(cb: (next: (e: undefined | Error | null, data: T) => void) => void, timeout?: number): Promise<T> {
  return new Promise((resolve: Resolve<T>, reject: Reject) => {
    const startTimer = (() => {
      if (timeout) {
        const localTimer = setTimeout(() => reject(new TimeoutError(`timeout after ${timeout} ms`)));
        return () => clearTimeout(localTimer);
      } else {
        return () => {};
      }
    })();
    const next = (e: undefined | Error | null, data: T) => {
      if (e) { return reject(e); } else { return resolve(data); }
      // else { return reject(new Error('there is no error, but there is no data either'))}
      // if (e) { reject(e); } else { resolve(data); };
      startTimer();
    };
    cb(next);
  });
}

export function withRetry<T>(limit: number, callback: () => Promise<T>, retryCount: number = 0, error?: Error): Promise<T> {
  if (retryCount >= limit) { throw error; }
  return callback().catch((err: Error) => {
    return withRetry(limit, callback, retryCount + 1, err);
  });
}

export function waitEvent<T>(emitter: EventEmitter, eventName: string, tolerance: number= 10000, filter?: (event: T) => boolean): Promise<T> {
  let listener: Resolve<T>;
  const eventFilter = filter ? filter : () => true;
  const eventPromise = new Promise((resolve: Resolve<T>, reject: Reject) => {
    listener = (event: T) => {
      if (eventFilter(event)) { resolve(event); }
    };
    emitter.once(eventName, listener);
  });
  const resultPromise = withTimeoutError(eventPromise, tolerance);
  resultPromise.then(() => emitter.removeListener(eventName, listener)).catch(() => {});
  resultPromise.catch(() => {}).then(() => emitter.removeListener(eventName, listener));
  return resultPromise;
}

export function decorateTimeoutError<I, O>(callback: FN<I, O>, tolerance: number = 10000): FN<I, Promise<O>> {
  return async (i1: I, ...args: any[]): Promise<O> => {
    const task = (async () => callback(i1, ...args));
    return wrapTimeoutError(task, tolerance);
  };
}

export function wrapTimeoutError<T>(callback: () => Promise<T>, tolerance: number = 10000): Promise<T> {
  return withTimeoutError(callback(), tolerance);
}

export function sleep(timeout: number): Promise<void> {
  return new Promise((resolve: Resolve<void>, reject: Reject) => {
    setTimeout(resolve, timeout);
  });
}

export function withTimeoutError<T>(promise: Promise<T>, tolerance: number = 10000): Promise<T> {
  let timeout: NodeJS.Timeout;
  const timeoutPromise = new Promise((rs: Resolve<T>, rj: Reject) => {
    timeout = setTimeout(() => {
      clearTimeout(timeout);
      rj(new Error(`Timedout`));
    }, tolerance);
  }) as Promise<T>;
  promise.then(() => clearTimeout(timeout)).catch(() => clearTimeout(timeout));
  timeoutPromise.catch(() => {}); // we dont need you to be noisy (?);
  return Promise.race([promise, timeoutPromise]);
}
