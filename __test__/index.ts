import chai, { assert, expect } from 'chai';
import EventEmitter from 'events';
import mlog from 'mocha-logger';
import { q, qd, waitEvent } from '../src/index';

describe('q', () => {
  context('without timeout', () => {
    it('can throw error', async () => {
      const cb = () => q((next) => next(new Error('fuck'), null));
      const afterCalled = await qd(cb);
      expect(afterCalled.error).not.to.eq(null);
      if (!afterCalled.error) { throw new Error('that was unexpected'); }
      expect(afterCalled.error.message).to.eq('fuck');
    });
    it('can return a result', async () => {
      const cb = () => q((next) => next(null, 'yes'));
      const afterCalled = await qd(cb);
      expect(afterCalled.result).to.eq('yes');
    });
  });
});

describe('waitEvent', () => {
  it('can wait event', async function(): Promise<void> {
    const ctx = this as Mocha.Context;
    ctx.timeout(400);
    const emitter = new EventEmitter();
    const eventName = 'Bad Day';
    mlog.log(`waiting for event ${eventName}`);
    const promise = waitEvent(emitter, eventName, 200);
    emitter.emit(eventName, 'OK');
    const result = await promise;
    mlog.log(`event emitted ${eventName}: ${result}`);
    expect(result).to.eq('OK');
  });
  it('can timeout', async function(): Promise<void> {
    const ctx = this as Mocha.Context;
    ctx.timeout(400);
    const emitter = new EventEmitter();
    const eventName = 'Bad Day';
    mlog.log(`waiting for event ${eventName}`);
    const promise = waitEvent(emitter, eventName, 200);
    const result = await promise.catch((error: Error) => error);
    expect(result).to.be.instanceOf(Error);
    expect(emitter.eventNames().length).to.eq(0);
    mlog.log(emitter.eventNames().join(' '));
    mlog.log('event timed out');
  });
});
